#
# CAS Dockerfile
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

# Pull base image.
FROM debian:jessie

MAINTAINER Baptiste MOINE <contact@bmoine.fr>

# Variables
ENV TOMCAT8_USER=tomcat \
    TOMCAT8_GROUP=tomcat \
    CATALINA_BASE=/var/tomcat/inst2 \
    CATALINA_BASE_ARCHIVE=var-tomcat-inst2.tar.bz2 \
    CATALINA_HOME=/usr/local/apache-tomcat-8.5.12 \
    CATALINA_HOME_ARCHIVE=usr-local-tomcat.tar.bz2 \
    CATALINA_PID=/var/run/tomcat/catalina-inst2.pid \
    CATALINA_TMPDIR=/tmp/tomcat8-tmp/ \
    JAVA_HOME=/usr/java/jdk1.8.0_121 \
    JAVA_HOME_ARCHIVE=usr-java-jdk8u121.tar.bz2 \
    APR_ARCHIVE=usr-local-apr.tar.bz2 \
    MAVEN3_VERSION=3.3.9 \
    CAS_ARCHIVE=etc-chl-cas5.tar.bz2

# Optional proxy configuration.
ENV HTTP_PROXY=http://172.18.4.1:8080 \
    HTTPS_PROXY=http://172.18.4.1:8080 \
    http_proxy=http://172.18.4.1:8080 \
    https_proxy=http://172.18.4.1:8080 \
    JAVA_FLAGS="-Dhttp.proxyHost=172.18.4.1 -Dhttp.proxyPort=8080"

COPY ./conf/apt/70debconf /etc/apt/apt.conf.d/70debconf

# Install minimal tools.
RUN apt-get update && apt-get install -y \
        curl \
        wget \
        git \
        subversion \
        mercurial \
        ntpdate \
        authbind \
        bzip2

# Compilation tools
RUN apt-get install -y \
        gcc \
        gcc-multilib \
        g++ \
        g++-multilib \
        bison \
        flex \
        autoconf \
        automake \
        build-essential

# Copy binaries.
RUN mkdir -p /tmp/bin/
COPY ./bin /tmp/bin/

# Install Apache Tomcat.
RUN export DIR=$(printf "%s" ${CATALINA_BASE} | awk -F'/' '{out=""; for (i=2; i<=NF-1; ++i){out=out""FS""$i}; print out}') \
    && mkdir -p ${DIR} \
    && cd ${DIR} \
    && tar xvjf /tmp/bin/${CATALINA_BASE_ARCHIVE} \
    && groupadd --system "${TOMCAT8_GROUP}" \
    && useradd --system --home-dir ${DIR} --no-create-home --gid "${TOMCAT8_GROUP}" --shell /bin/false "${TOMCAT8_USER}" \
    && install -m 750 -o ${TOMCAT8_USER} -g adm -d /var/cache/tomcat8/ \
    && install -m 750 -o ${TOMCAT8_USER} -g adm -d /var/log/tomcat8/ \
    && install -m 750 -o ${TOMCAT8_USER} -g adm -d $(printf "%s" ${CATALINA_PID} | awk -F'/' '{out=""; for (i=2; i<=NF-1; ++i){out=out""FS""$i}; print out}') \
    && rm -rf ${CATALINA_BASE}/temp \
    && rm -rf ${CATALINA_BASE}/logs \
    && rm -rf ${CATALINA_BASE}/work \
    && ln -s ${CATALINA_TMPDIR} ${CATALINA_BASE}/temp \
    && ln -s /var/log/tomcat8/ ${CATALINA_BASE}/logs \
    && ln -s /var/cache/tomcat8/ ${CATALINA_BASE}/work \
    && chown -Rh ${TOMCAT8_USER}:${TOMCAT8_GROUP} ${CATALINA_BASE} \
    && export DIR=$(printf "%s" ${CATALINA_HOME} | awk -F'/' '{out=""; for (i=2; i<=NF-1; ++i){out=out""FS""$i}; print out}') \
    && mkdir -p ${DIR} \
    && cd ${DIR} \
    && tar xvjf /tmp/bin/${CATALINA_HOME_ARCHIVE}

# Install Oracle Java.
RUN export DIR=$(printf "%s" ${JAVA_HOME} | awk -F'/' '{out=""; for (i=2; i<=NF-1; ++i){out=out""FS""$i}; print out}') \
    && mkdir -p ${DIR} \
    && cd ${DIR} \
    && tar xvjf /tmp/bin/${JAVA_HOME_ARCHIVE}

# Install Apache Portable Runtime.
# RUN export DIR=$(printf "%s" ${APR_ARCHIVE} | awk -F'-' '{out=""; for (i=1; i<=NF-1; ++i){out=out"/"$i}; print out}') \
#     && mkdir -p ${DIR} \
#     && cd ${DIR} \
#     && tar xvjf /tmp/bin/${APR_ARCHIVE}

# Install Apache Portable Runtime.
# RUN export DIR="/usr/local/apr" \
#     && cd /tmp/ \
#     && wget http://mirrors.ircam.fr/pub/apache/apr/apr-1.5.2.tar.bz2 \
#     && tar xvjf apr-1.5.2.tar.bz2 \
#     && cd apr-1.5.2/ \
#     && CC="gcc -m64" ./configure --prefix=${DIR} \
#     && make \
#     && make test \
#     && make install

# Install Apache Maven.
RUN cd /tmp/ \
    && wget http://apache.mirrors.ovh.net/ftp.apache.org/dist/maven/maven-3/${MAVEN3_VERSION}/binaries/apache-maven-${MAVEN3_VERSION}-bin.tar.gz \
    && tar xvzf apache-maven-${MAVEN3_VERSION}-bin.tar.gz --directory /opt/
COPY ./conf/maven/settings.xml /root/.m2/settings.xml
ENV PATH=/opt/apache-maven-3.3.9/bin:${PATH}

# Build Central Authentication Service.
ADD ./conf/cas/ /opt/cas-server-5/
RUN cd /opt/cas-server-5/ \
    && mvn clean package -e \
    && cp ./target/cas.war ${CATALINA_BASE}/webapps/

# Install Central Authentication Service.
RUN export DIR=$(printf "%s" ${CAS_ARCHIVE} | awk -F'-' '{out=""; for (i=1; i<=NF-1; ++i){out=out"/"$i}; print out}') \
    && mkdir -p ${DIR} \
    && cd ${DIR} \
    && tar xvjf /tmp/bin/${CAS_ARCHIVE}

# Clean.
RUN rm -rf /tmp/* \
    && apt-get clean

# Create volumes.
VOLUME ["/var/log/tomcat8/"]

# UDP/TCP port that container will listen for connections.
EXPOSE 8080/tcp

# Copy startup script.
COPY ./start.sh /start.sh
RUN chmod +x /start.sh

CMD ["bash", "-c", "/start.sh"]
