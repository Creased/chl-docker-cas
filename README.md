Conteneur Docker pour CAS
=========================

## Setup ##

### Prérequis ###

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/).

### Téléchargement ###

```bash
git clone https://gitlab.miletrie.chl/MB082668/chl-docker-cas docker-cas
pushd docker-cas/
git submodule init
git submodule update
```

### Build ###

Fabrication du conteneur basée sur [docker-compose.yml](docker-compose.yml)&nbsp;:

```bash
docker-compose pull
docker-compose build
```

## Run ##

Pour démarrer le conteneur, procédez comme suit&nbsp;:

```bash
docker-compose create
docker-compose start
docker-compose up
```

## Affichage en continue des logs ##

```bash
docker-compose logs --follow
```

## Exécution d'un bash dans le conteneur ##

Syntaxe&nbsp;:

```bash
docker-compose exec SERVICE COMMAND
```

Exemple&nbsp;:

```bash
docker-compose exec db bash
```
