# Informations utiles

- Utilisateur non privilégié&nbsp;: `tomcat`
- Répertoire personnel&nbsp;: `/var/tomcat`
- `cas.war` est à déposer dans `/var/tomcat/inst2/webapps/`
- Script de lancement de Apache Tomcat &nbsp;: `/var/tomcat/tomcat-8.5.12-java-8.0.121.sh start`

| Fichier                   | Décompresser dans | Description                                                 |
|---------------------------|-------------------|-------------------------------------------------------------|
| etc-chl-cas5.tar.bz2      | /etc/chl          | Configuration de CASv5 à laisser à l'extérieur du conteneur |
| usr-java-jdk8u121.tar.bz2 | /usr/java         | Oracle Java JDK 1.8u121                                     |
| usr-local-apr.tar.bz2     | /usr/local        | Apache Portable Runtime 1.5.2 - Librairies                  |
| usr-local-tomcat.tar.bz2  | /usr/local        | Apache Tomcat 8.5.12                                        |
| var-tomcat-inst2.tar.bz2  | /var/tomcat       | Instance de tomcat, dossiers de travail                     |
