#!/bin/bash

###
# Tomcat
#
rm -rf ${CATALINA_TMPDIR}
SCRIPT="$(printf "%s" ${CATALINA_BASE} | awk -F'/' '{out=""; for (i=2; i<=NF-1; ++i){out=out""FS""$i}; print out}')/tomcat-8.5.12-java-8.0.121.sh"
install -o ${TOMCAT8_USER} -g ${TOMCAT8_GROUP} -d ${CATALINA_TMPDIR}
chown -Rh ${TOMCAT8_USER}:${TOMCAT8_GROUP} ${CATALINA_BASE}
chown ${TOMCAT8_USER}:${TOMCAT8_GROUP} /var/log/tomcat8/
start-stop-daemon --start -u "${TOMCAT8_USER}" -g "${TOMCAT8_GROUP}" -c "${TOMCAT8_USER}" -d "${CATALINA_TMPDIR}" -x /bin/bash -- -c "/usr/bin/authbind --deep /bin/bash -c '${SCRIPT} start'"
while ! tail -f /var/log/tomcat8/*; do sleep 2; done
